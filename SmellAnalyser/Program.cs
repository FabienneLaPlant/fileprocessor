﻿using System;
using System.Collections.Generic;

namespace SmellAnalyser {
    class Program {

        private static FileOperations fileOperations = new FileOperations();
        private static RuleEngine ruleEngine = new RuleEngine();
        private static Analytics analytics = new Analytics();

        private static string inputFile = "";
        private static string outputFile = "";
        private static string ExpressionsFile = "";
        private static string categorisationFile = "";
        private static string mode = "";

        private static ICollection<string> data = new List<string>();

        static void Main(string[] args) {

            ProcessArguments(args);
            if (inputFile.Length == 0) {
                return;
            }

            data = fileOperations.ReadLinesFromFile(inputFile);
            data = CleanupAndUnify(data);
            data = Categorize(data);

            switch (mode) {
                case "CSL": // CountSimilarLines
                    Dictionary<string, int> result = analytics.CountSimilarLines(data);
                    fileOperations.WriteResultsToFile(result, outputFile);
                    break;

                case "CIPC":
                    Dictionary<string, int> CIPC_Result = analytics.CountIssuesInProjectsAndClasses_SONAR(data);
                    fileOperations.WriteResultsToFile(CIPC_Result, outputFile);
                    break;
                default:
                    fileOperations.WriteResultsToFile(data, outputFile);
                    break;
            }

        }


        private static ICollection<string> Categorize(ICollection<string> data) {
            if (categorisationFile.Length != 0) {
                ICollection<string> categorisations = fileOperations.ReadLinesFromFile(categorisationFile);
                data = ruleEngine.ApplyRules(data, categorisations);
            }

            return data;
        }

        private static ICollection<string> CleanupAndUnify(ICollection<string> data) {
            if (ExpressionsFile.Length != 0) {
                ICollection<string> regexList = fileOperations.ReadLinesFromFile(ExpressionsFile);
                data = ruleEngine.ApplyRules(data, regexList);
            }

            return data;
        }

        private static void ProcessArguments(string[] args) {
            foreach (string parameter in args)
            {
                if (parameter.Contains("-in=")) {
                    inputFile = parameter.Substring(4);
                }

                if (parameter.Contains("-out=")) {
                    outputFile = parameter.Substring(5);
                }

                if (parameter.Contains("-ef=")) {
                    ExpressionsFile = parameter.Substring(4);
                }

                if (parameter.Contains("-cf=")) {
                    categorisationFile = parameter.Substring(4);
                }

                if (parameter.Contains("-mode=")) {
                    mode = parameter.Substring(6);
                }
            }
        }
    }
}

