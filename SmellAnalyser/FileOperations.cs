﻿using System;
using System.Collections.Generic;
using System.IO;

namespace SmellAnalyser {
    public class FileOperations {
        public List<string> ReadLinesFromFile(String filename) {
            List<string> result = new List<string>();
            string line;

            StreamReader file = new StreamReader(filename);
            while ((line = file.ReadLine()) != null) {
                result.Add(line);
            }

            file.Close();
            return result;
        }


        public string ReadFile(string filename) {
            return File.ReadAllText(filename);
        }

        public void WriteResultsToFile(Dictionary<string, int> results, string filename) {
            StreamWriter file = new StreamWriter(filename);
            foreach (string key in results.Keys) {
                file.WriteLine(results[key] + ";" + key);
            }
            file.Close();
        }


        public void WriteResultsToFile(ICollection<string> results, string filename) {
            StreamWriter file = new StreamWriter(filename);
            foreach (string line in results) {
                file.WriteLine(line);
            }
            file.Close();
        }
    }
}
