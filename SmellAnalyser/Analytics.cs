﻿using System.Collections.Generic;

namespace SmellAnalyser {
    public class Analytics {
        public Dictionary<string, int> CountSimilarLines(ICollection<string> data) {
            Dictionary<string, int> result = new Dictionary<string, int>();
            return Count(data, result);
        }

        public Dictionary<string, int> CountIssuesInProjectsAndClasses_SONAR(ICollection<string> data) {
            Dictionary<string, int> result = new Dictionary<string, int>();
            ICollection<string> splittedLines = new List<string>();

            foreach (string line in data) {
                string[] splitted = line.Split('/');
                splittedLines.Add(splitted[0]);
                splittedLines.Add(splitted[splitted.Length - 1]);
            }

            return Count(splittedLines, result);
        }

        private static Dictionary<string, int> Count(ICollection<string> data, Dictionary<string, int> result) {
            foreach (string line in data) {
                if (result.TryGetValue(line, out int count)) {
                    result[line] = count + 1;
                }
                else
                    result.Add(line, 1);
            }
            return result;
        }
    }
}
