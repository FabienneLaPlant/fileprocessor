﻿using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;

namespace SmellAnalyser {
    public class RuleEngine {

        public ICollection<string> ApplyRules(ICollection<string> data, ICollection<string> regexList) {
            foreach (string regex in regexList) {
                data = ApplyRegex(data, regex);
            }
            data = removeEmptyLines(data);
            return data;
        }

        private ICollection<string> removeEmptyLines(ICollection<string> data) {
            ICollection<string> result = new List<string>();
            foreach (string line in data) {
                if(line.Length > 0) {
                    result.Add(line);
                }
            }
            return result;
        }

        private ICollection<string> ApplyRegex(ICollection<string> data, string regex) {
            ICollection<string> result = new List<string>();

            if(ReplaceCommand(regex, ref data, out result)) {
                return result;
            }

            if(ExistsCommand(regex, ref data, out result)) {
                return result;
            }

            if (MergeCommand(regex, ref data, out result)) {
                return result;
            }

            return null;
        }


        private bool ReplaceCommand(string Expression, ref ICollection<string> input, out ICollection<string> output) {
            List<string> result = new List<string>();
            if (Expression.Contains("replace§")) {
                string[] RegexAndReplacement = SplitExpression(Expression, new[]{ '§', '|' });
                
                foreach (string line in input) {
                    result.Add(Regex.Replace(line, RegexAndReplacement[0], RegexAndReplacement[1]));
                }
            }
            output = result;
            return result.Count != 0;
        }


        private bool MergeCommand(string regex, ref ICollection<string> input, out ICollection<string> output) {
            ICollection<string> result = new List<string>();
            if (regex.Contains("merge§")) {
                string[] separators = SplitExpression(regex, new[] { '§', ',' });

                string line = string.Join(separators[0], input);
                result = new List<string>(line.Split(new[] { separators[1] }, StringSplitOptions.None));
            }
            output = result;
            return output.Count != 0;
        }


        protected bool ExistsCommand(string regex, ref ICollection<string> data, out ICollection<string> output) {
            ICollection<string> result = new List<string>();

            if (regex.Contains("exists§")) {
                string[] searchFor = SplitExpression(regex, new[] {'§', ','});

                foreach (string line in data) {
                    foreach (string element in searchFor) {
                        if (line.Contains(element)) {
                            result.Add(line);
                        }
                    }
                }
            }
            output = result;
            return output.Count != 0;
        }


        protected static string[] SplitExpression(string regex, char[] splittingSign) {
            regex = regex.Split(splittingSign[0])[1];
            return regex.Split(splittingSign[1]);            
        }
    }
}
