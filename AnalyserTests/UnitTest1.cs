﻿using System;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using SmellAnalyser;

namespace AnalyserTests {    
    [TestClass]
    public class UnitTest1 {

        private const string readTestFile = @"C:\Users\Frederik Wendt\source\repos\SmellAnalyser\AnalyserTests\TestFiles\readFileTest.txt";
        private const string similarLinesFile = @"C:\Users\Frederik Wendt\source\repos\SmellAnalyser\AnalyserTests\TestFiles\countSimilarLines.txt";


        [TestMethod]
        public void readLinesTest() {
            FileOperations fileOperations = new FileOperations();
            RuleEngine analyser = new RuleEngine();
            List<string> result = fileOperations.ReadLinesFromFile(readTestFile);
            List<string> expected = new List<string> {
                "a", "b", "c", "d", "  ", "e", "f", "11"
            };

            Assert.AreEqual(expected.Count, result.Count);
            for(int i = 0; i< result.Count; i++) {
                Assert.AreEqual(expected[i], result[i]);
            }
        }

        [TestMethod]
        public void CountSimilarLines() {
            Analytics analytics = new Analytics();
            FileOperations fileOperations = new FileOperations();
            RuleEngine analyser = new RuleEngine();
            List<string> data = fileOperations.ReadLinesFromFile(similarLinesFile);
            Dictionary<string, int> sortedData = analytics.CountSimilarLines(data);

            sortedData.TryGetValue("aa", out int count);
            Assert.AreEqual(3, count);

            sortedData.TryGetValue("bb", out count);
            Assert.AreEqual(2, count);

            sortedData.TryGetValue("s", out count);
            Assert.AreEqual(1, count);

            sortedData.TryGetValue("", out count);
            Assert.AreEqual(3, count);

            sortedData.TryGetValue("sd", out count);
            Assert.AreEqual(1, count);

            sortedData.TryGetValue("sad", out count);
            Assert.AreEqual(1, count);

            sortedData.TryGetValue("  ", out count);
            Assert.AreEqual(3, count);

            sortedData.TryGetValue("     ", out count);
            Assert.AreEqual(1, count);

            sortedData.TryGetValue("asdadasd", out count);
            Assert.AreEqual(1, count);            
        }
    }
}
